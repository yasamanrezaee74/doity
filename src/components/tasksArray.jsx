export const tasks = [
  {
    _id: "100100",
    title: "Check Email Inbox",
    createdDate: new Date().toISOString().split("T")[0],
    done: false,
    doneDate: "It isn't done yet",
    deadline: new Date().toISOString().split("T")[0]
  },
  {
    _id: "101100",
    title: "Answer All Emails",
    createdDate: new Date().toISOString().split("T")[0],
    done: false,

    doneDate: "It isn't done yet",
    deadline: new Date().toISOString().split("T")[0]
  },
  {
    _id: "102100",
    title: "Write a Report",
    createdDate: new Date().toISOString().split("T")[0],
    done: false,

    doneDate: "It isn't done yet",
    deadline: new Date().toISOString().split("T")[0]
  },
  {
    _id: "103100",
    title: "Mid-day Meeting",
    createdDate: new Date().toISOString().split("T")[0],
    done: false,

    doneDate: "It isn't done yet",
    deadline: new Date().toISOString().split("T")[0]
  },
  {
    _id: "104100",
    title: "Read latest Articles",
    createdDate: new Date().toISOString().split("T")[0],
    doneDate: "It isn't done yet",
    deadline: new Date().toISOString().split("T")[0]
  },
  {
    _id: "105100",
    title: "Buy Tomato",
    createdDate: new Date().toISOString().split("T")[0],
    done: false,

    doneDate: "It isn't done yet",
    deadline: new Date().toISOString().split("T")[0]
  },
  {
    _id: "106100",
    title: "Make Dinner",
    createdDate: new Date().toISOString().split("T")[0],
    done: false,

    doneDate: "It isn't done yet",
    deadline: new Date().toISOString().split("T")[0]
  },
  {
    _id: "107100",
    title: "Go for a stroll",
    createdDate: new Date().toISOString().split("T")[0],
    done: false,

    doneDate: "It isn't done yet",
    deadline: new Date().toISOString().split("T")[0]
  }
];

export function getTask(id) {
  let task = tasks.filter(t => t._id === id)[0];
  return task;
  // for (let t in tasks) {
  //   if (id.toString() === t._id) {
  //     task = 2;
  //   }
  //   return task;
  // }
}

export function getTasks() {
  return tasks.filter(t => t);
}

export function saveTask(task) {
  if (tasks.indexOf(task) === -1) {
    tasks.push(task);
  }
  return task;
}

export function saveTasks(task) {
  if (tasks.indexOf(task) !== -1) {
    tasks[tasks.indexOf(task)] = task;
  }
  console.log("savetasks" + tasks);
  return tasks;
}
