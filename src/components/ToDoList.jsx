import React, { Component } from "react";
import Tasks from "./Tasks";
import AddToDo from "./AddToDo";
// import Toast from "./Toast";
import { toast } from "react-toastify";
import { getTasks, saveTask, saveTasks } from "./tasksArray";
import "./toDoList.css";

class ToDoList extends Component {
  state = {
    tasks: getTasks()
  };

  componentDidMount() {
    const tasks = JSON.parse(localStorage.getItem("tasks"));
    if (tasks) return this.setState({ tasks });
  }

  ontaskDetailsHandler = task => {
    this.props.history.push(`/taskdetails${task._id}`);
    localStorage.setItem("tasks", JSON.stringify(this.state.tasks));
  };

  doSubmitHandler = task => {
    const tasks = [...this.state.tasks];
    if (task.title && task.deadline) {
      tasks.push(task);
      this.setState({ tasks });
      localStorage.setItem("tasks", JSON.stringify(tasks));
      // return <Toast />;
      toast.success("Task's added successfully:)", {
        position: toast.POSITION.TOP_RIGHT,
        className: "foo-bar"
      });
    } else {
      // return <Toast />;

      toast.error("Please complete both fields!", {
        position: toast.POSITION.TOP_RIGHT,
        className: "foo-bar"
      });
    }
  };

  onDoneHandler = item => {
    const tasks = [...this.state.tasks];
    const taskIndex = this.state.tasks.indexOf(item);
    let task = { ...this.state.tasks[taskIndex] };
    task.done = !task.done;
    task.doneDate = new Date().toISOString().split("T")[0];
    console.log("done or ondone" + task.done);
    console.log("done" + task.doneDate);
    task.deadline = this.state.tasks[taskIndex].deadline;
    tasks[taskIndex] = task;
    saveTasks(task);
    this.setState({ tasks });
    console.log("done" + task.doneDate);
    localStorage.setItem("tasks", JSON.stringify(tasks));
    // console.log("ondone" + tasks);
  };

  onDeleteHandler = item => {
    const tasks = [...this.state.tasks];
    const taskIndex = this.state.tasks.indexOf(item);
    const task = { ...this.state.tasks[taskIndex] };
    tasks[taskIndex] = task;
    tasks.splice(taskIndex, 1);
    this.setState({ tasks });
    localStorage.setItem("tasks", JSON.stringify(tasks));
  };

  render() {
    // console.log("state taska" + this.state.tasks);
    return (
      <div className="container">
        <div className="row row-header">
          <div className="col-12">
            <h2>To Do List</h2>
          </div>
        </div>
        <AddToDo doSubmit={this.doSubmitHandler} />
        <div className="row  row-content">
          <div className="col-12">
            <Tasks
              ontaskDetails={this.ontaskDetailsHandler}
              onDelete={this.onDeleteHandler}
              onDone={this.onDoneHandler}
              items={this.state.tasks}
            />
          </div>
        </div>
      </div>
    );
  }
}

export default ToDoList;
