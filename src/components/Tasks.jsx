import React from "react";
import "./toDoList.css";
// import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";

const Tasks = props => {
  return (
    <ul className="list-group list-group-flush">
      {props.items.map(item => (
        <li key={item.title} className="list-group-item">
          <div className="row">
            <div className="col-1 check-style">
              {/* {!item.done && "\u20dd"} */}

              <span
                style={{ "font-size": "20px", cursor: "pointer" }}
                onClick={() => props.onDone(item)}
              >
                {item.done ? (
                  <i className="fa fa-check-square"></i>
                ) : (
                  <i className="fa fa-square"></i>
                )}
              </span>
            </div>
            <div className="col-7">
              <strong
                onClick={() => props.ontaskDetails(item)}
                className={item.done ? "title-style deadline" : "title-style"}
              >
                {item.title}
              </strong>
            </div>

            <div className={item.done ? "deadline" : ""}>
              Deadline: {item.deadline}
            </div>

            <div className="d-flex justify-content-center ml-auto col-2">
              <button
                onClick={() => props.onDelete(item)}
                className="align-self-start btn btn-danger"
              >
                Delete
              </button>
              {/* <button
                type="button"
                className="align-self-start btn btn-primary"
                data-toggle="modal"
                data-target="#exampleModal"
              >
                Edit
              </button> */}
            </div>
          </div>
        </li>
      ))}
    </ul>
  );
};

export default Tasks;
