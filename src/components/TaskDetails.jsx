import React, { Component } from "react";
import { saveTask, saveTasks, getTasks, getTask } from "./tasksArray";
import _ from "lodash";
import "./toDoList.css";

class TaskDetails extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      task:
        JSON.parse(localStorage.getItem("tasks")).filter(
          t => t._id === this.props.match.params.id.toString()
        )[0] || getTask(this.props.match.params.id.toString()),
      tasks: JSON.parse(localStorage.getItem("tasks"))
      //  ||
      // getTasks()
    };

    this.handleChange = this.handleChange.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
  }

  // componentDidMount() {
  //   const tasks = JSON.parse(localStorage.getItem("tasks"));
  //   if (tasks) return this.setState({ tasks });
  // }

  handleChange(event) {
    // if (JSON.parse(localStorage.getItem("tasks"))) {
    // const tasks = JSON.parse(localStorage.getItem("tasks"));
    const tasks = [...this.state.tasks];
    console.log("after" + tasks);
    let task = { ...this.state.task };
    // const task = getTask(this.props.match.params.title)[0];

    // const task = tasks.filter(
    //   t => t.title === this.props.match.params.title
    // )[0];
    console.log(task);
    const taskIndex = _.findIndex(tasks, task);
    console.log(taskIndex);
    task[event.target.name] = event.target.value;
    tasks[taskIndex] = task;
    console.log(tasks);
    this.setState({ task, tasks });
    // saveTask(task);
    // saveTasks(tasks);
    localStorage.setItem("tasks", JSON.stringify(tasks));
    // }
    //  else {
    //   let task = { ...this.state.task };
    //   console.log(task);
    //   task[event.target.name] = event.target.value;
    //   console.log(task);
    //   this.setState({ task });
    //   saveTasks(task);
    // }
    // const taskIndex = _.find(tasks, this.props.match.params.title);
    // console.log(taskIndex);
    // localStorage.setItem("tasks", JSON.stringify(tasks));
    // this.setState({ task });
  }

  handleSubmit(event) {
    event.preventDefault();
    this.props.history.replace("/");
  }

  render() {
    console.log(this.props.match.params.id);
    return (
      <div>
        <div className="row-content">
          <h2 className="row-header">{this.state.task.title}</h2>
          <p>Created Date: {this.state.task.createdDate}</p>
          <p>Done Date: {this.state.task.doneDate}</p>
        </div>
        <form onSubmit={this.handleSubmit}>
          <div className="row form-group row-content">
            <div className="col-12 row row-input">
              <label>
                Task Title
                <input
                  className="form-control input-margin"
                  type="text"
                  value={this.state.task.title}
                  onChange={this.handleChange}
                  name="title"
                />
              </label>
            </div>
            <div className="col-12 row row-input">
              <label>
                Deadline
                <input
                  className="form-control"
                  type="date"
                  value={this.state.task.deadline}
                  onChange={this.handleChange}
                  name="deadline"
                />
              </label>
            </div>
            <input
              className="ml-auto col-1 align-self-end btn btn-success btn-sm add-button-style"
              type="submit"
              value="Save"
            />
          </div>
        </form>
      </div>
    );
  }
}

export default TaskDetails;
