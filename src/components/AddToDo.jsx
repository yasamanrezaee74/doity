import React, { Component } from "react";
import { saveTask, getTasks } from "./tasksArray";
import "./toDoList.css";

class ToDoForm extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      task: {
        _id: "",
        title: "",
        createdDate: "",
        done: false,
        doneDate: "It isn't done yet",
        deadline: ""
      }
    };

    this.handleChange = this.handleChange.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
  }

  handleChange(event) {
    const task = this.state.task;
    task[event.target.name] = event.target.value;
    task.createdDate = new Date().toISOString().split("T")[0];
    task._id = new Date().getMilliseconds().toString();
    task.done = this.state.task.done;
    // if (this.state.task.done) {
    //   task.doneDate = new Date().toISOString().split("T")[0];
    // }

    this.setState({ task });
    console.log(this.state.task);
    // console.log(event.target.value);
  }

  handleSubmit(event) {
    saveTask(this.state.task);
    event.preventDefault();
    // console.log(getTasks());
    this.setState({
      task: {
        _id: "",
        title: "",
        createdDate: "",
        done: false,
        doneDate: "It isn't done yet"
      }
    });
    this.props.doSubmit(this.state.task);
  }

  render() {
    return (
      <form onSubmit={this.handleSubmit}>
        <div className="row form-group">
          <div className="col-12 col-lg-4">
            <label>
              Task Title
              <input
                className="form-control input-margin"
                type="text"
                value={this.state.task.title}
                onChange={this.handleChange}
                name="title"
              />
            </label>
          </div>
          <div className="col-12 col-lg-3">
            <label>
              Deadline
              <input
                className="form-control"
                type="date"
                value={this.state.task.deadline}
                onChange={this.handleChange}
                name="deadline"
              />
            </label>
          </div>
          {/* <div className="col-12 col-lg-3">
            <label>
              Done Date
              <input
                className="form-control"
                type="date"
                value={this.state.task.doneDate}
                onChange={this.handleChange}
                name="doneDate"
              />
            </label>
          </div> */}
          <input
            className="ml-auto align-self-end btn btn-success btn-sm add-button-style"
            type="submit"
            value="+"
          />
        </div>
      </form>
    );
  }
}
export default ToDoForm;
