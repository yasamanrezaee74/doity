import React from "react";
import { Route, Redirect, Switch } from "react-router-dom";
import ToDoList from "../components/ToDoList";
import TaskDetails from "../components/TaskDetails";
import { toast } from "react-toastify";
import "react-toastify/dist/ReactToastify.css";
import "./App.css";

function App() {
  toast.configure({
    autoClose: 8000,
    draggable: false
    //etc you get the idea
  });
  return (
    <main className="container">
      <Switch>
        <Route path="/taskdetails:id" component={TaskDetails} />
        <Route path="/todolist" component={ToDoList} />
        <Redirect from="/" exact to="/todolist" />
      </Switch>
    </main>
  );
}

export default App;
